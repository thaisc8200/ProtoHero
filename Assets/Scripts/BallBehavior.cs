﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehavior : MonoBehaviour
{
    private Vector3 m_Position;
    public float Speed;
    // Start is called before the first frame update
    void Start()
    {
        m_Position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager._instance.gameState == GameStates.playing)
        {
            m_Position.x += Speed * GameManager._instance.Multiplier;
            transform.position = m_Position;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("End"))
        {
            Destroy(gameObject);
            GameManager._instance.errors++;
        }
    }
}
